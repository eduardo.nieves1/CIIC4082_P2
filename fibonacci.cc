#include <pthread.h>
#include <stdio.h>
#include <stdlib.h>

int fib;
int *seq;
void * fibonacci_thread(void * n){
   int a = 0, b = 1, c, i;
   seq[0] = 0;
   seq[1] = 1;
   for ( i = 1; i < (int) n ; i++){
      seq[i + 1] = a + b;
      a = b;
      b = seq[i + 1];
   }
   printf("\n");
   pthread_exit(0);
}

int main(int argc, char **argv){
   int i, n = atoi(argv[1]);
   seq = calloc(n, sizeof(int));
   pthread_t tid;
   pthread_attr_t attr;
   pthread_attr_init(&attr);
   pthread_create(&tid,&attr,fibonacci_thread, (void *) n);
   pthread_join(tid, NULL);
   printf("The Fibonacci Sequence for the entered number (%d) is:\n", atoi(argv[1]));
   for (i = 0; i < n; i++)
      printf("%d\t",seq[i]);
   printf("\n");
   free(seq);
   return 0;
}
