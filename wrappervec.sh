#!/bin/bash

for bandwidth in {256..4096..256}
do
    for num_cores in {2..20..2}
    do
	./wrapper-mem.sh $num_cores $bandwidth
	for threads in {2..7}
	do
	    num_threads=$((2**$threads))
	    ./wrapper-cpu.sh  $num_cores 1
	    for vector in $(eval echo {$threads..8})
	    do
		vector_size=$((2**$vector))
		echo "Run :  Cores-$num_cores Threads-$num_threads Vector size-$vector_size Bandwidth-$bandwidth"
		echo "Bandwidth : $bandwidth"
		echo "Vector Product:" 
                m2s  --x86-config multicore-config --mem-config multicore-mem-config.txt --x86-report x86-cpu-report-vecprod-Cores-${num_cores}-Threads-${num_threads}-Vector-size-${vector_size}-Bandwidth-${bandwidth} \
		    --mem-report x86-mem-report-vecprod-Cores-${num_cores}-Threads-${num_threads}-Vector-size-${vector_size}-Bandwidth-${bandwidth} --x86-sim detailed vecprod $num_threads $vector_size
	    done  
	done
    done
done